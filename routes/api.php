<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::post('auth/login', 'MobileController@authLogin');
Route::post('permissions', 'MobileController@permissions');
Route::post('submit-report', 'MobileController@submitReport');
Route::post('submit-complaint', 'MobileController@submitComplaint');
Route::post('push-notification', 'MobileController@pushNotification');
Route::post('home-carousell', 'MobileController@homeCarousell');
Route::post('dashboard-charts', 'MobileController@dashboardCharts');