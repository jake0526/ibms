<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// GET Requests Guest
Route::get('/', 'PagesController@index');

//POST Requests Guest
// Route::post('/api/auth/login', 'MobileController@authLogin');

Auth::routes();

// GET Requests Authenticated
Route::get('/home', 'AuthenticatedController@index');

Route::get('/user-management', 'AuthenticatedController@userManagement');
Route::get('/complaints', 'AuthenticatedController@complaints')
    ->middleware('complaint');
Route::get('/reports', 'AuthenticatedController@reports')
    ->middleware('report');
Route::get('/admin/dashboard/reports', 'AuthenticatedController@adminDashboardReports');
Route::get('/admin/dashboard/reports-crime', 'AuthenticatedController@adminDashboardReportsCrime');
Route::get('/admin/dashboard/budget', 'AuthenticatedController@adminDashboardBudget');
Route::get('/admin/dashboard/crime', 'AuthenticatedController@adminDashboardCrime');
Route::get('/admin/dashboard/population', 'AuthenticatedController@adminDashboardPopulation');
Route::get('/admin/announcement', 'AuthenticatedController@adminAnnouncement');
Route::get('/admin/user-permission', 'AuthenticatedController@adminUserAndPermission');

Route::get('/user/profile', 'AuthenticatedController@userProfile');

Route::group(['middleware' => ['admin']], function () {
    Route::get('/admin/dashboard', 'AuthenticatedController@adminDashboard');
});
// Route::get('/administrator', 'PagesController@login');

//POST Requests Authenticated
Route::post('/api/insert-user', 'PostController@insertUser');
Route::post('/api/update-user', 'PostController@updateUser');
Route::post('/api/delete-user', 'PostController@deleteUser');
Route::post('/api/change-password', 'PostController@changePassword');
Route::post('/api/make-schedule', 'PostController@makeSchedule');

Route::post('/api/assign-permission', 'PostController@assignPermission');
Route::post('/api/delete-user-permission', 'PostController@deleteUserPermission');

Route::post('/api/insert-announcement', 'PostController@insertAnnouncement');
Route::post('/api/display-announcement', 'PostController@displayAnnouncement');
Route::post('/api/delete-announcement', 'PostController@deleteAnnouncement');
Route::post('/api/update-announcement', 'PostController@updateAnnouncement');

Route::post('/api/check-permission', 'PostController@checkPermission');

Route::post('/api/add-budget', 'PostController@addBudget');
Route::post('/api/update-budget', 'PostController@updateBudget');
Route::post('/api/delete-budget', 'PostController@deleteBudget');

Route::post('/api/add-crime', 'PostController@addCrime');
Route::post('/api/update-crime', 'PostController@updateCrime');
Route::post('/api/delete-crime', 'PostController@deleteCrime');

Route::post('/api/add-population', 'PostController@addPopulation');
Route::post('/api/update-population', 'PostController@updatePopulation');
Route::post('/api/delete-population', 'PostController@deletePopulation');

Route::post('/api/upload-images', 'PostController@uploadImages');

Route::post('/api/get-notification', 'PostController@getNotification');
Route::post('/api/get-report-by-days', 'PostController@getReportByDays');