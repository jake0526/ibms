<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

use App\HomeCarousell;

class HomeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $homeCarousell = new HomeCarousell;

        $homeCarousell->announcement_id = 1;
        $homeCarousell->image1 = 'images/img1.jpeg';
        $homeCarousell->image2 = 'images/img2.jpg';
        $homeCarousell->image3 = 'images/img3.jpg';
        $homeCarousell->save();
    }
}
