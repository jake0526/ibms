<?php

use Illuminate\Database\Seeder;
use App\User;
use App\UserPermission;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // Insert Admin Start
        $user = new User;

        $user->name = "Firstname Lastname";
        $user->firstname = "Firstname";
        $user->middlename = "Middlename";
        $user->lastname = "Lastname";
        $user->birthdate = "2018-11-18";
        $user->username = "admin";
        $user->email = "admin@gmail.com";
        $user->password = Hash::make("secret");
        $user->address = "Davao City";
        $user->save();
        // Insert Admin End

        // Insert Admin Permission Start
        $userPermission = new UserPermission;

        $userPermission->user_id = 1;
        $userPermission->permission_id = 1;
        $userPermission->description = "";
        $userPermission->effect = "allow";
        $userPermission->action = "read";
        $userPermission->save();

        $userPermission = new UserPermission;

        $userPermission->user_id = 1;
        $userPermission->permission_id = 1;
        $userPermission->description = "";
        $userPermission->effect = "allow";
        $userPermission->action = "write";
        $userPermission->save();
        // Insert Admin Permission End
    }
}
