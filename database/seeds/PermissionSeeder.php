<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

use App\Permission;

class PermissionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $permission = new Permission;

        $permission->name = "Admin";
        $permission->description = "Has access to all permissions";
        $permission->save();

        $permission = new Permission;

        $permission->name = "Complaints";
        $permission->description = "Complaints";
        $permission->save();

        $permission = new Permission;

        $permission->name = "Incident Report";
        $permission->description = "Incident Report";
        $permission->save();
    }
}
