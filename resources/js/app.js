
/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');
require('./chart.min');
// var svm = require('svm');

import Vue from 'vue';
import BootstrapVue from 'bootstrap-vue';
import 'bootstrap-vue/dist/bootstrap-vue.css';
import fullCalendar from 'vue-fullcalendar';
import * as VueGoogleMaps from 'vue2-google-maps';
import VueColor from 'vue-color/dist/vue-color.js';
import VueMonthlyPicker from 'vue-monthly-picker';
import {MonthPicker} from 'vue-month-picker';


Vue.use(VueGoogleMaps, {
    load: {
      key: 'AIzaSyAMOKCatRDmqhzpRFbOHRa1TINdFM45BEQ',
      libraries: 'places', // This is required if you use the Autocomplete plugin
      // OR: libraries: 'places,drawing'
      // OR: libraries: 'places,drawing,visualization'
      // (as you require)
   
      //// If you want to set the version, you can do so:
      // v: '3.26',
    },
   
    //// If you intend to programmatically custom event listener code
    //// (e.g. `this.$refs.gmap.$on('zoom_changed', someFunc)`)
    //// instead of going through Vue templates (e.g. `<GmapMap @zoom_changed="someFunc">`)
    //// you might need to turn this on.
    // autobindAllEvents: false,
   
    //// If you want to manually install components, e.g.
    //// import {GmapMarker} from 'vue2-google-maps/src/components/marker'
    //// Vue.component('GmapMarker', GmapMarker)
    //// then disable the following:
    // installComponents: true,
})

Vue.use(BootstrapVue);
/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */

Vue.component('example-component', require('./components/ExampleComponent.vue'));
Vue.component('navbar-main', require('./components/layout/NavbarMain.vue'));
Vue.component('navbar-manager-dashboard', require('./components/layout/manager/NavbarDashboard.vue'));
Vue.component('sidebar-manager-dashboard', require('./components/layout/manager/SidebarDashboard.vue'));
Vue.component('navbar-admin-dashboard', require('./components/layout/administrator/NavbarDashboard.vue'));
Vue.component('sidebar-admin-dashboard', require('./components/layout/administrator/SidebarDashboard.vue'));
Vue.component('complaints-main-tab', require('./components/complaints/MainTab.vue'));
Vue.component('reports-main-tab', require('./components/reports/MainTab.vue'));
Vue.component('user-permission', require('./components/administrator/UserPermission.vue'));
Vue.component('announcement', require('./components/administrator/Announcement.vue'));
Vue.component('profile-component', require('./components/ProfileComponent.vue'));
Vue.component('dashboard-budget', require('./components/administrator/dashboard/Budget.vue'));
Vue.component('dashboard-crime', require('./components/administrator/dashboard/Crime.vue'));
Vue.component('dashboard-population', require('./components/administrator/dashboard/Population.vue'));
Vue.component('notification', require('./components/layout/Notification.vue'));
Vue.component('notification-dash', require('./components/layout/NotificationDash.vue'));

/**
 * Node Components
 */
Vue.component('full-calendar', fullCalendar);
Vue.component('vue-monthly-picker', VueMonthlyPicker);
Vue.component('month-picker', MonthPicker);
Vue.component('google-map', require('./components/reports/MainMap.vue'))

var Chrome = VueColor.Chrome;
Vue.component('colorpicker', {
	components: {
		'chrome-picker': Chrome,
	},
    template: `
    <div class="input-group" ref="colorpicker">
        <div class="input-group-prepend">
            <span class="input-group-text current-color" :style="'background-color: ' + color" @click="togglePicker()"></span>
            <chrome-picker :value="colors" @input="updateFromPicker" v-if="displayPicker" />
        </div>
        <input type="text" class="form-control" v-model="color" @focus="showPicker()" @input="updateFromInput">
    </div>`,
	props: ['color'],
	data() {
		return {
			colors: {
				hex: '#000000',
			},
			colorValue: '',
			displayPicker: false,
		}
	},
	mounted() {
		this.setColor(this.color || '#000000');
	},
	methods: {
		setColor(color) {
            console.log(color);

			this.updateColors(color);
			this.colorValue = color;
		},
		updateColors(color) {
			if(color.slice(0, 1) == '#') {
				this.colors = {
					hex: color
				};
			}
			else if(color.slice(0, 4) == 'rgba') {
				var rgba = color.replace(/^rgba?\(|\s+|\)$/g,'').split(','),
					hex = '#' + ((1 << 24) + (parseInt(rgba[0]) << 16) + (parseInt(rgba[1]) << 8) + parseInt(rgba[2])).toString(16).slice(1);
				this.colors = {
					hex: hex,
					a: rgba[3],
				}
			}
		},
		showPicker() {
			document.addEventListener('click', this.documentClick);
			this.displayPicker = true;
		},
		hidePicker() {
			document.removeEventListener('click', this.documentClick);
			this.displayPicker = false;
		},
		togglePicker() {
			this.displayPicker ? this.hidePicker() : this.showPicker();
		},
		updateFromInput() {
			this.updateColors(this.colorValue);
		},
		updateFromPicker(color) {
			this.colors = color;
			if(color.rgba.a == 1) {
				this.colorValue = color.hex;
			}
			else {
				this.colorValue = 'rgba(' + color.rgba.r + ', ' + color.rgba.g + ', ' + color.rgba.b + ', ' + color.rgba.a + ')';
			}
		},
		documentClick(e) {
			var el = this.$refs.colorpicker,
				target = e.target;
			if(el !== target && !el.contains(target)) {
				this.hidePicker()
			}
		}
	},
	watch: {
		colorValue(val) {
			if(val) {
				this.updateColors(val);
				this.$emit('input', val);
				//document.body.style.background = val;
			}
		}
	},
});

Vue.use(require('vue-moment'));

const app = new Vue({
    el: '#app',
});