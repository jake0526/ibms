<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <script> window.Laravel = { csrfToken: '{{ csrf_token() }}'} </script>

    <!-- Fonts -->
    <link rel="dns-prefetch" href="https://fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet" type="text/css">

    <!-- Styles -->
    <link rel="stylesheet" href="{{asset('css/app.css')}}">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.4.2/css/all.css" integrity="sha384-/rXc/GQVaYpyDdyxK+ecHPVYJSN9bmVFBvjA/9eOB+pb3F2w2N6fc5qB9Ew5yIns" crossorigin="anonymous">
    <link rel="stylesheet" href="https://cdn.datatables.net/1.10.19/css/jquery.dataTables.min.css">

    {{-- <title> test </title> --}}
    <title>{{ config('app.name', 'IBMS') }} | {{$title}}</title>
</head>
<body>
    <div id="app">
        {{-- <nav class="navbar navbar-expand-md navbar-light navbar-laravel">
            <div class="container">
                <a class="navbar-brand" href="{{ url('/') }}">
                    {{ config('app.name', 'Laravel') }}
                </a>
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="{{ __('Toggle navigation') }}">
                    <span class="navbar-toggler-icon"></span>
                </button>

                <div class="collapse navbar-collapse" id="navbarSupportedContent">
                    <!-- Left Side Of Navbar -->
                    <ul class="navbar-nav mr-auto">

                    </ul>

                    <!-- Right Side Of Navbar -->
                    <ul class="navbar-nav ml-auto">
                        <!-- Authentication Links -->
                        @guest
                            <li class="nav-item">
                                <a class="nav-link" href="{{ route('login') }}">{{ __('Login') }}</a>
                            </li>
                            <li class="nav-item">
                                @if (Route::has('register'))
                                    <a class="nav-link" href="{{ route('register') }}">{{ __('Register') }}</a>
                                @endif
                            </li>
                        @else
                            <li class="nav-item dropdown">
                                <a id="navbarDropdown" class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                                    {{ Auth::user()->name }} <span class="caret"></span>
                                </a>

                                <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
                                    <a class="dropdown-item" href="{{ route('logout') }}"
                                       onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                        {{ __('Logout') }}
                                    </a>

                                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                        @csrf
                                    </form>
                                </div>
                            </li>
                        @endguest
                    </ul>
                </div>
            </div>
        </nav> --}}

        <main>
            @yield('content')
        </main>
    </div>
    <script src="{{asset('js/app.js')}}"> </script>
    <script src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"> </script>

    <script type='text/javascript'>
        // Call the dataTables jQuery plugin
        $(document).ready(function() {
            $('#dataTable').DataTable({
                "aaSorting": []
            });
        });

        // Set new default font family and font color to mimic Bootstrap's default styling
        Chart.defaults.global.defaultFontFamily = '-apple-system,system-ui,BlinkMacSystemFont,"Segoe UI",Roboto,"Helvetica Neue",Arial,sans-serif';
        Chart.defaults.global.defaultFontColor = '#292b2c';

        // Report ComplaintsPie

        var labelList = [];
        var valueList = [];
        var colorList = [];

        console.log(JSON.parse('<?php echo $reports; ?>'));

        <?php
        $colorDefaultList = array(  
            'Noise' => '#66a3ff',
            'Water Interruption' => '#d966ff',
            'Electrical Interruption' => '#c266ff',
            'Flood' => '#f442dc',
            'Others' => '#ff66a3'
        );

        foreach ($reports as $key=>$item) {
            if($key == 'Noise' || $key == 'Water Interruption' || $key == 'Electrical Interruption' || $key == 'Flood' || $key == 'Others'){
                echo "labelList.push('" . $key . "');";
                echo "valueList.push('" . count($item) . "');";
                echo "colorList.push('" . $colorDefaultList[$key] . "');";
            }
        }
        ?>

        console.log(labelList);
        console.log(valueList);
        console.log(colorList);

        var ctx = document.getElementById("reportsComplaintsPie");
        var reportsComplaintsPie = new Chart(ctx, {
            type: 'pie',
            data: {
                labels: labelList,
                datasets: [{
                data: valueList,
                backgroundColor: colorList,
                }],
            },
        });

        // Reported Crimes Pie

        var labelList = [];
        var valueList = [];
        var colorList = [];

        <?php
        $colorDefaultList = array( 
            'Violence' => '#8cd98c',
            'Rape' => '#258c10',
            'Robbery' => '#737c71',
            'Drugs' => '#078269',
            'Murder' => '#071582'
        );

        foreach ($reports as $key=>$item) {
            if($key == 'Violence' || $key == 'Rape' || $key == 'Robbery' || $key == 'Drugs' || $key == 'Murder'){
                echo "labelList.push('" . $key . "');";
                echo "valueList.push('" . count($item) . "');";
                echo "colorList.push('" . $colorDefaultList[$key] . "');";
            }
        }
        ?>

        console.log(labelList);
        console.log(valueList);
        console.log(colorList);

        var ctx = document.getElementById("reportsCrimePie");
        var reportsCrimePie = new Chart(ctx, {
            type: 'pie',
            data: {
                labels: labelList,
                datasets: [{
                data: valueList,
                backgroundColor: colorList,
                }],
            },
        });

        // Pie Chart Example
        console.log("main");

        var labelList = [];
        var valueList = [];
        var colorList = [];

        <?php
        foreach ($budget as $item) {
            echo "labelList.push('" . $item->label . "');";
            echo "valueList.push('" . $item->value . "');";
            echo "colorList.push('" . $item->color . "');";
        }
        ?>

        var ctx = document.getElementById("myPieChart");
        var myPieChart = new Chart(ctx, {
            type: 'pie',
            data: {
                labels: labelList,
                datasets: [{
                data: valueList,
                backgroundColor: colorList,
                }],
            },
        });

        // Bar Chart Example
        var labelListCrime = [];
        var valueListCrime = [];
        var colorListCrime = [];

        <?php
        foreach ($crime as $item) {
            echo "labelListCrime.push('" . $item->label . "');";
            echo "valueListCrime.push('" . $item->value . "');";
            echo "colorListCrime.push('" . $item->color . "');";
        }
        ?>

        var ctx = document.getElementById("myBarChart");
        var myLineChart = new Chart(ctx, {
        type: 'bar',
        data: {
            labels: labelListCrime,
            datasets: [{
            label: "Record",
            backgroundColor: colorListCrime,
            data: valueListCrime,
            }],
        },
        options: {
            scales: {
            xAxes: [{
                time: {
                unit: 'Crime'
                },
                gridLines: {
                display: false
                },
                ticks: {
                maxTicksLimit: 6
                }
            }],
            yAxes: [{
                ticks: {
                min: 0,
                max: 200,
                maxTicksLimit: 5
                },
                gridLines: {
                display: true
                }
            }],
            },
            legend: {
            display: false
            }
        }
        });

        // Area Chart Example
        var labelListPopulation = [];
        var valueListPopulation = [];
        var colorListPopulation = [];

        <?php
        foreach ($population as $item) {
            echo "labelListPopulation.push('" . $item->label . "');";
            echo "valueListPopulation.push('" . $item->value . "');";
            echo "colorListPopulation.push('" . $item->color . "');";
        }
        ?>

        var ctx = document.getElementById("myAreaChart");
        var myLineChart = new Chart(ctx, {
        type: 'line',
        data: {
            labels: labelListPopulation,
            datasets: [{
                label: "Population",
                lineTension: 0.3,
                data: valueListPopulation,
                backgroundColor: "rgba(2,117,216,0.2)",
                borderColor: colorListPopulation,
                pointRadius: 5,
                pointBackgroundColor: colorListPopulation,
                pointBorderColor: "rgba(255,255,255,0.8)",
                pointHoverRadius: 5,
                pointHoverBackgroundColor: colorListPopulation,
                pointHitRadius: 50,
                pointBorderWidth: 2,
            }],
        },
        options: {
            scales: {
            xAxes: [{
                time: {
                unit: 'date'
                },
                gridLines: {
                display: false
                },
                ticks: {
                maxTicksLimit: 7
                }
            }],
            yAxes: [{
                ticks: {
                min: 0,
                max: 500000,
                maxTicksLimit: 5
                },
                gridLines: {
                color: "rgba(0, 0, 0, .125)",
                }
            }],
            },
            legend: {
            display: false
            }
        }
        });
    </script>
</body>
</html>
