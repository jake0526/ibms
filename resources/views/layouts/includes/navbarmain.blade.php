<nav class="navbar navbar-expand-lg navbar-ibms">
    <a class="navbar-brand" href="/">
        <img class="logo-dash navbarnavbar-brand" src="/images/logo.png" /> BRGY 101
    </a>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
    </button>
    
    <div class="collapse navbar-collapse" id="navbarSupportedContent">
        <ul class="navbar-nav mr-auto">
            <li class="nav-item">
                
            </li>
        </ul>
        @guest
            <ul class="nav navbar-nav navbar-right">
                <li class="nav-item">
                    <a class="nav-link" href="/login">Login</a>
                </li>
            </ul>
        @else
            <notification notification="{{$notification}}"> </notification>
            <ul class="nav navbar-nav navbar-right">
                <li class="nav-item dropdown">
                    <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        {{ Auth::user()->firstname }}
                    </a>
                    <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
                        <a class="dropdown-item" href="/user/profile">Profile</a>
                        @if($isAdmin)
                        <a class="dropdown-item" href="/admin/dashboard">Administrator</a>
                        <a class="dropdown-item" href="/complaints">Manager</a>
                        @else
                            @if($hasComplaint)
                                <a class="dropdown-item" href="/complaints">Manager</a>
                            @elseif($hasReport)
                                <a class="dropdown-item" href="/reports">Manager</a>
                            @endif
                        @endif
                        <div class="dropdown-divider"></div>
                        <a class="dropdown-item" href="{{ route('logout') }}"
                         onclick="event.preventDefault();
                         document.getElementById('logout-form').submit();">
                            Logout
                        </a>

                        <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                            @csrf
                        </form>
                    </div>
                </li>
            </ul>
        @endguest
    </div>
</nav>