@extends('layouts.app')

@section('content')
@include('layouts.includes.navbarmanager')
<div class="container-fluid">
    <div class="row">
        <sidebar-manager-dashboard page="{{$page}}"> </sidebar-manager-dashboard>

        <main role="main" class="col-md-9 ml-sm-auto col-lg-10 px-4">
            <div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3 border-bottom">
                <h1 class="h2">User Management</h1>
                <div class="btn-toolbar mb-2 mb-md-0">
                    <div class="btn-group mr-2">
                        <button class="btn btn-sm btn-outline-secondary">Share</button>
                        <button class="btn btn-sm btn-outline-secondary">Export</button>
                    </div>
                    <button class="btn btn-sm btn-outline-secondary dropdown-toggle">
                        <span data-feather="calendar"></span>
                        This week
                    </button>
                </div>
            </div>
            <nav aria-label="breadcrumb">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="#">Home</a></li>
                    <li class="breadcrumb-item active" aria-current="page">User Management</li>
                </ol>
            </nav>
            <hr />

            <div class="card mb-3">
                <div class="card-header">
                    <i class="fas fa-table"></i>
                    Users
                </div>
                <div class="card-body">
                    <button type="button" class="btn btn-outline-primary" data-toggle="modal" data-target="#addUserModal">Add</button>
                    <br /> <br /> 
                    <div class="table-responsive">
                    <table class="table table-bordered" id="userDataTable" width="100%" cellspacing="0">
                        <thead>
                            <tr>
                                <th>Username</th>
                                <th>Firstname</th>
                                <th>Lastname</th>
                                <th>User Type</th>
                                <th>Age</th>
                                <th>Address</th>
                                <th>Date Registered</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                        <tfoot>
                            <tr>
                                <th>Username</th>
                                <th>Firstname</th>
                                <th>Lastname</th>
                                <th>User Type</th>
                                <th>Age</th>
                                <th>Address</th>
                                <th>Date Registered</th>
                                <th>Action</th>
                            </tr>
                        </tfoot>
                        <tbody>
                            <tr>
                                <td>jennie</td>
                                <td>Jennie</td>
                                <td>Kim</td>
                                <td>Admin</td>
                                <td>22</td>
                                <td>New Zealand</td>
                                <td>2018/10/28</td>
                                <td> 
                                    <a href="#"> <button type="button" class="btn btn-outline-primary"> <span class="fas fa-pen-alt"></span> </button> </a> 
                                    <a href="#"> <button type="button" class="btn btn-outline-danger"> <span class="fas fa-times"></span> </button> </a> 
                                    <a href="#"> <button type="button" class="btn btn-outline-info"> <span class="fas fa-info"></span> </button> </a>
                                </td>
                            </tr>
                            <tr>
                                <td>robert</td>
                                <td>Robert Geanne</td>
                                <td>Ricarte</td>
                                <td>Sub-admin</td>
                                <td>20</td>
                                <td>P2 B5 L16 Teachers Village Talomo, Davao City</td>
                                <td>2018/10/28</td>
                                <td> 
                                    <a href="#"> <button type="button" class="btn btn-outline-primary"> <span class="fas fa-pen-alt"></span> </button> </a> 
                                    <a href="#"> <button type="button" class="btn btn-outline-danger"> <span class="fas fa-times"></span> </button> </a>
                                    <a href="#"> <button type="button" class="btn btn-outline-info"> <span class="fas fa-info"></span> </button> </a>
                                </td>
                            </tr>
                            <tr>
                                <td>christine</td>
                                <td>Christine Joy</td>
                                <td>Masula</td>
                                <td>Sub-admin</td>
                                <td>20</td>
                                <td>DBP Maa, Davao City</td>
                                <td>2018/10/28</td>
                                <td> 
                                    <a href="#"> <button type="button" class="btn btn-outline-primary"> <span class="fas fa-pen-alt"></span> </button> </a> 
                                    <a href="#"> <button type="button" class="btn btn-outline-danger"> <span class="fas fa-times"></span> </button> </a>
                                    <a href="#"> <button type="button" class="btn btn-outline-info"> <span class="fas fa-info"></span> </button> </a>
                                </td>
                            </tr>
                            <tr>
                                <td>arvin</td>
                                <td>Arvinjill</td>
                                <td>Oregenes</td>
                                <td>Sub-admin</td>
                                <td>20</td>
                                <td>Matina Gravahan Matina, Davao City</td>
                                <td>2018/10/28</td>
                                <td> 
                                    <a href="#"> <button type="button" class="btn btn-outline-primary"> <span class="fas fa-pen-alt"></span> </button> </a>  
                                    <a href="#"> <button type="button" class="btn btn-outline-danger"> <span class="fas fa-times"></span> </button> </a>      
                                    <a href="#"> <button type="button" class="btn btn-outline-info"> <span class="fas fa-info"></span> </button> </a>                          
                                </td>
                            </tr>
                            <tr>
                                <td>somerset</td>
                                <td>Somerset Elcid</td>
                                <td>Siang</td>
                                <td>User</td>
                                <td>20</td>
                                <td>Davao City</td>
                                <td>2018/10/28</td>
                                <td> 
                                    <a href="#"> <button type="button" class="btn btn-outline-primary"> <span class="fas fa-pen-alt"></span> </button> </a>  
                                    <a href="#"> <button type="button" class="btn btn-outline-danger"> <span class="fas fa-times"></span> </button> </a>
                                    <a href="#"> <button type="button" class="btn btn-outline-info"> <span class="fas fa-info"></span> </button> </a>
                                </td>
                            </tr>
                        </tbody> 
                    </table>
                    </div>
                </div>
                <div class="card-footer small text-muted">Updated yesterday at 11:59 PM</div>
            </div>
        </main>
    </div>
</div>

<div class="modal fade" id="addUserModal" tabindex="-1" role="dialog" aria-labelledby="addUserModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
        <div class="modal-header">
            <h5 class="modal-title" id="exampleModalLabel">Add User</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
            </button>
        </div>
        <div class="modal-body">
            <form>
                <div class="form-group">
                    <div class="form-group">
                        <div class="form-label-group">
                        <input type="text" id="username" class="form-control" placeholder="Username" required="required">
                        </div>
                    </div>
                    <div class="form-row">
                        <div class="col-sm-6">
                            <div class="form-label-group">
                            <input type="text" id="firstName" class="form-control" placeholder="First name" required="required" autofocus="autofocus">
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="form-label-group">
                            <input type="text" id="lastName" class="form-control" placeholder="Last name" required="required">
                            </div>
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <div class="form-row">
                        <div class="col-sm-6">
                            <div class="form-label-group">
                            <input type="number" id="age" class="form-control" placeholder="Age" required="required">
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="form-label-group">
                            <input type="text" id="address" class="form-control" placeholder="Address" required="required">
                            </div>
                        </div>
                    </div>
                </div>
            </form>
        </div>
        <div class="modal-footer">
            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
            <button type="button" class="btn btn-primary">Save changes</button>
        </div>
        </div>
    </div>
</div>
@endsection