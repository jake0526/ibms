@extends('layouts.app')

@section('content')
@include('layouts.includes.navbarmanager')
<div class="container-fluid">
    <div class="row">
        <sidebar-manager-dashboard page="{{$page}}"> </sidebar-manager-dashboard>

        <main role="main" class="col-md-9 ml-sm-auto col-lg-10 px-4">
            <div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3 border-bottom">
                <h1 class="h2">Reports</h1>
                <div class="btn-toolbar mb-2 mb-md-0">
                    <div class="btn-group mr-2">
                        <button class="btn btn-sm btn-outline-secondary">Share</button>
                        <button class="btn btn-sm btn-outline-secondary">Export</button>
                    </div>
                    <button class="btn btn-sm btn-outline-secondary dropdown-toggle">
                        <span data-feather="calendar"></span>
                        This week
                    </button>
                </div>
            </div>
            <nav aria-label="breadcrumb">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="#">Home</a></li>
                    <li class="breadcrumb-item active" aria-current="page">Reports</li>
                </ol>
            </nav>
            <hr />

        <reports-main-tab report-month="{{$reportMonth}}" report-year="{{$reportYear}}" report-data="{{$reportData}}"> </reports-main-tab>
        </main>
    </div>
</div>
@endsection