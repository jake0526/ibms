@extends('layouts.app')

@section('content')
@include('layouts.includes.navbaradmin')
<div class="container-fluid">
    <div class="row">
        <sidebar-admin-dashboard page="{{$page}}"> </sidebar-admin-dashboard>

        <main role="main" class="col-md-9 ml-sm-auto col-lg-10 px-4">
            <div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3 border-bottom">
                <h1 class="h2">
                    <br />
                    Citizen Reported Crimes Chart
                </h1>
            </div>
            <nav aria-label="breadcrumb">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"> <a href="/admin/dashboard"> Dashboard </a></li>
                    <li class="breadcrumb-item active" aria-current="page">Reports</a></li>
                </ol>
            </nav>
            <hr />

            <div class="card mb-3">
                <div class="card-header">
                    <i class="fas fa-table"></i>
                    Citizen Reported Crimes Chart
                </div>
                
                <canvas id="reportsCrimePie" width="100%" height="100"></canvas>
            </div>

            {{-- <div class="row">
                <div class="col-sm-12">
                    <div class="card o-hidden h-100">
                        <div class="card-body">
                            <div class="card-body-icon">
                            </div>
                            <div class="mr-5">Budget</div>
                        </div>
                        
                        <canvas id="myPieChart" width="100%" height="100"></canvas>

                        <a class="card-footer clearfix small z-1" href="/admin/dashboard/budget">
                            <span class="float-left">View Details</span>
                            <span class="float-right">
                            <i class="fas fa-angle-right"></i>
                            </span>
                        </a>
                    </div>
                </div>
            </div> --}}
        </main>
    </div>
</div>
@endsection