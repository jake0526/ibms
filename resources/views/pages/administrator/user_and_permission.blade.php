@extends('layouts.app')

@section('content')
@include('layouts.includes.navbaradmin')
<div class="container-fluid">
    <div class="row">
        <sidebar-admin-dashboard page="{{$page}}"> </sidebar-admin-dashboard>

        <main role="main" class="col-md-9 ml-sm-auto col-lg-10 px-4">
            <div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3 border-bottom">
                <h1 class="h2">
                    <br />
                    Manage User and Permission
                </h1>
            </div>
            <nav aria-label="breadcrumb">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="/admin/dashboard">Dashboard</a></li>
                    <li class="breadcrumb-item active" aria-current="page">Manage User and Permission</li>
                </ol>
            </nav>
            <hr />

            <div class="card mb-3">
                <div class="card-header">
                    <i class="fas fa-table"></i>
                    Users
                </div>
                <user-permission user-data="{{$userData}}" permission-data="{{$permissions}}"> </user-permission-table>
                <div class="card-footer small text-muted">Updated yesterday at 11:59 PM</div>
            </div>
        </main>
    </div>
</div>
@endsection