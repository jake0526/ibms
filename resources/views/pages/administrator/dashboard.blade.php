@extends('layouts.app')

@section('content')
@include('layouts.includes.navbaradmin')
<div class="container-fluid">
    <div class="row">
        <sidebar-admin-dashboard page="{{$page}}"> </sidebar-admin-dashboard>

        <main role="main" class="col-md-9 ml-sm-auto col-lg-10 px-4">
            <div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3 border-bottom">
                <h1 class="h2">
                    <br />
                    Dashboard
                </h1>
                {{-- <div class="btn-toolbar mb-2 mb-md-0">
                    <div class="btn-group mr-2">
                        <button class="btn btn-sm btn-outline-secondary">Share</button>
                        <button class="btn btn-sm btn-outline-secondary">Export</button>
                    </div>
                    <button class="btn btn-sm btn-outline-secondary dropdown-toggle">
                        <span data-feather="calendar"></span>
                        This week
                    </button>
                </div> --}}
            </div>
            <nav aria-label="breadcrumb">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item active" aria-current="page">Dashboard</li>
                </ol>
            </nav>
            <hr />
            Filter by days: 
            <div class="dropdown">
                <button class="btn btn-primary dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    {{$reportDays}} Days
                </button>
                <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                    <a class="dropdown-item" href="/admin/dashboard?d=7">7 Days</a>
                    <a class="dropdown-item" href="/admin/dashboard?d=30">30 Days</a>
                </div>
            </div> <br />
            <div class="row">
                <div class="col-sm-6">
                    <div class="card o-hidden h-100">
                        <div class="card-body">
                            <div class="card-body-icon">
                            <i class="fas fa-bullhorn"></i>
                            </div>
                            <div class="mr-5">Citizen Complaints</div>
                        </div>
                        
                        <canvas id="reportsComplaintsPie" width="100%" height="100"></canvas>

                        {{-- <a class="card-footer clearfix small z-1" href="/admin/dashboard/reports">
                            <span class="float-left">View Details</span>
                            <span class="float-right">
                            <i class="fas fa-angle-right"></i>
                            </span>
                        </a> --}}
                    </div>
                </div>
                <div class="col-sm-6">
                    <div class="card o-hidden h-100">
                        <div class="card-body">
                            <div class="card-body-icon">
                            <i class="fas fa-bullhorn"></i>
                            </div>
                            <div class="mr-5">Citizen Reported Crimes</div>
                        </div>
                        
                        <canvas id="reportsCrimePie" width="100%" height="100"></canvas>

                        {{-- <a class="card-footer clearfix small z-1" href="/admin/dashboard/reports-crime">
                            <span class="float-left">View Details</span>
                            <span class="float-right">
                            <i class="fas fa-angle-right"></i>
                            </span>
                        </a> --}}
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-sm-4">
                    <div class="card o-hidden h-100">
                        <div class="card-body">
                            <div class="card-body-icon">
                            <i class="fas fa-fw fa-money-bill-alt"></i>
                            </div>
                            <div class="mr-5">Budget</div>
                        </div>
                        
                        <canvas id="myPieChart" width="100%" height="100"></canvas>

                        <a class="card-footer clearfix small z-1" href="/admin/dashboard/budget">
                            <span class="float-left">View Details</span>
                            <span class="float-right">
                            <i class="fas fa-angle-right"></i>
                            </span>
                        </a>
                    </div>
                </div>
                <div class="col-sm-4">
                    <div class="card o-hidden h-100">
                        <div class="card-body">
                            <div class="card-body-icon">
                            <i class="fas fa-fw fa-running"></i>
                            </div>
                            <div class="mr-5">PNP Recorded Crimes</div>
                        </div>

                        <canvas id="myBarChart" width="100%" height="100"></canvas>

                        <a class="card-footer clearfix small z-1" href="/admin/dashboard/crime">
                            <span class="float-left">View Details</span>
                            <span class="float-right">
                            <i class="fas fa-angle-right"></i>
                            </span>
                        </a>
                    </div>
                </div>
                <div class="col-sm-4">
                    <div class="card o-hidden h-100">
                        <div class="card-body">
                            <div class="card-body-icon">
                            <i class="fas fa-fw fa-users"></i>
                            </div>
                            <div class="mr-5">Population</div>
                        </div>

                        <canvas id="myAreaChart" width="100%" height="100"></canvas>

                        <a class="card-footer clearfix small z-1" href="/admin/dashboard/population">
                            <span class="float-left">View Details</span>
                            <span class="float-right">
                            <i class="fas fa-angle-right"></i>
                            </span>
                        </a>
                    </div>
                </div>
            </div>
        </main>
    </div>
</div>
@endsection