@extends('layouts.app')
@section('content')
@include('layouts.includes.navbarmain')
<div class="container" style="margin-top: 50px;">
    <div id="carouselExampleControls" class="carousel slide" data-ride="carousel">
        <ol class="carousel-indicators">
            <li data-target="#carouselExampleIndicators" data-slide-to="0" class="active"></li>
            <li data-target="#carouselExampleIndicators" data-slide-to="1"></li>
            <li data-target="#carouselExampleIndicators" data-slide-to="2"></li>
        </ol>
        <div class="carousel-inner">
          <div class="carousel-item active">
            <img class="d-block w-100" src="{{$homeController->image1}}" alt="First slide" style="width: 100%; height: 500px">
          </div>
          <div class="carousel-item">
            <img class="d-block w-100" src="{{$homeController->image2}}" alt="Second slide" style="width: 100%; height: 500px">
          </div>
          <div class="carousel-item">
            <img class="d-block w-100" src="{{$homeController->image3}}" alt="Third slide" style="width: 100%; height: 500px">
          </div>
        </div>
        <a class="carousel-control-prev" href="#carouselExampleControls" role="button" data-slide="prev">
          <span class="carousel-control-prev-icon" aria-hidden="true"></span>
          <span class="sr-only">Previous</span>
        </a>
        <a class="carousel-control-next" href="#carouselExampleControls" role="button" data-slide="next">
          <span class="carousel-control-next-icon" aria-hidden="true"></span>
          <span class="sr-only">Next</span>
        </a>
    </div>

    <div class="jumbotron p-3 p-md-5 text-white rounded bg-dark">
        <div class="col-md-6 px-0">
          <h1 class="display-4 font-italic">Announcement</h1>
          <p class="lead my-3">
            {{$homeController->announcement->content}}
          </p>
          <p class="lead mb-0"><a href="#" class="text-white font-weight-bold">Continue reading...</a></p>
        </div>
    </div>
</div>
@endsection