<!doctype html>
<html lang="{{ app()->getLocale() }}">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <meta name="csrf-token" content="{{ csrf_token() }}">
        <script> window.Laravel = { csrfToken: '{{ csrf_token() }}'} </script>

        <link rel="stylesheet" href="{{asset('css/app.css')}}">
        <link rel="stylesheet" href="{{asset('css/login.css')}}">

        <link rel="stylesheet" href="https://cdn.datatables.net/1.10.19/css/jquery.dataTables.min.css">

        <title>{{ config('app.name', 'IBMS') }} | {{$title}}</title>
    </head>
    <body class="text-center">
        <form class="form-signin" method="POST" action="{{ route('login') }}">
            @csrf
            {{-- <img class="mb-4" src="../../assets/brand/bootstrap-solid.svg" alt="" width="72" height="72"> --}}
            <img class="mb-5 login-logo" src="/images/logo.png" />
            {{-- <h1 class="mb-3 font-weight-normal">BRGY 101</h1> --}}
            <label for="username" class="sr-only">Username</label>
            <input type="text" id="username" class="form-control" name="username" placeholder="Username" value="{{ old('username') }}" required autofocus>
            <label for="password" class="sr-only">Password</label>
            <input type="password" id="password" class="form-control" placeholder="Password" name="password" required>
            @if ($errors->has('username'))
                <div class="alert alert-danger" role="alert">
                    {{ $errors->first('username') }}
                </div>
            @endif
            <div class="checkbox mb-3">
                <input class="form-check-input" type="checkbox" name="remember" id="remember" {{ old('remember') ? 'checked' : '' }}>

                <label class="form-check-label" for="remember">
                    {{ __('Remember Me') }}
                </label>
            </div>
            <button class="btn btn-lg btn-primary btn-block" type="submit">{{ __('Login') }}</button>
            {{-- <a href="/">
                <button class="btn btn-lg btn-primary btn-block" type="button">Sign in</button>
            </a> --}}
            <p class="mt-5 mb-3 text-muted">&copy; 2017-2018</p>
        </form>
        <script src="{{asset('js/app.js')}}"> </script>

        <script src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"> </script>
    </body>
</html>