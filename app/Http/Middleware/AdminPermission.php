<?php

namespace App\Http\Middleware;

use Closure;
use Auth;

use App\User;

class AdminPermission
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $userPermission = User::where('id', Auth::user()->id)->with('userPermissions.permission')->first();

        foreach ($userPermission->userPermissions as $value) {
            if ($value->permission->name == 'Admin') {
                return $next($request);
            }
        }

        return redirect('/');
    }
}
