<?php

namespace App\Http\Middleware;

use Closure;
use Auth;

use App\User;

class IncidentReport
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (Auth::user()) {
            $userPermission = User::where('id', Auth::user()->id)->with('userPermissions.permission')->first();

            foreach ($userPermission->userPermissions as $value) {
                if ($value->permission->name == 'Admin') {
                    return $next($request);
                }else if($value->permission->name == 'Incident Report') {
                    return $next($request);
                }
            }
        }

        return redirect('/');
    }
}
