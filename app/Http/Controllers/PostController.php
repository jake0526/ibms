<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use App\UserPermission;
use App\Permission;
use Hash;
use Auth;
use App\Complaint;
use Illuminate\Support\Carbon;
use App\Announcement;
use App\HomeCarousell;
use App\DashboardCrime;
use App\DashboardBudget;
use App\DashboardPopulation;
use App\Notification;
use App\Report;

class PostController extends Controller
{
    public function insertUser(Request $request) {

        $userData = $request->get('userData');
        
        $user = new User;

        $user->name = $userData['firstname'] . ' ' . $userData['middlename'] . ' ' . $userData['lastname'];
        $user->firstname = $userData['firstname'];
        $user->middlename = $userData['middlename'];
        $user->lastname = $userData['lastname'];
        $user->birthdate = $userData['birthdate'];
        $user->username = $userData['username'];
        $user->email = $userData['email'];
        $user->password = Hash::make("123456");
        $user->address = $userData['address'];
        $user->save();

        $returnData = User::with('userPermissions.permission')->get();

        return response()->json($returnData);
    }

    public function updateUser(Request $request) {

        $userData = $request->get('userData');
        
        $user = User::find($userData['id']);

        $user->name = $userData['firstname'] . ' ' . $userData['middlename'] . ' ' . $userData['lastname'];
        $user->firstname = $userData['firstname'];
        $user->middlename = $userData['middlename'];
        $user->lastname = $userData['lastname'];
        $user->birthdate = $userData['birthdate'];
        $user->username = $userData['username'];
        $user->email = $userData['email'];
        if ($userData['password'] != "") {
            $user->password = Hash::make($userData['password']);
        }
        $user->address = $userData['address'];
        $user->save();

        $returnData = User::with('userPermissions.permission')->get();

        return response()->json($returnData);
    }

    public function deleteUser(Request $request) {

        $userID = $request->get('userID');

        User::destroy($userID);

        $returnData = User::with('userPermissions.permission')->get();

        return response()->json($returnData);
    }

    public function assignPermission(Request $request) {

        $userID = $request->get('userID');
        $permissionID = $request->get('permissionID');
        $action = $request->get('action');

        $userPermission = new UserPermission;

        $userPermission->user_id = $userID;
        $userPermission->permission_id = $permissionID;
        $userPermission->description = "";
        $userPermission->effect = "allow";
        $userPermission->action = $action;
        $userPermission->save();

        $returnData = User::with('userPermissions.permission')->get();

        return response()->json($returnData);
    }

    public function deleteUserPermission(Request $request) {

        UserPermission::destroy($request->get('userPermissionID'));

        $returnData = User::with('userPermissions.permission')->get();

        return response()->json($returnData);
    }

    public function checkPermission(Request $request) {
        
        $userPermission = User::where('id', Auth::user()->id)->with('userPermissions.permission')->first();

        foreach ($userPermission->userPermissions as $value) {
            if ($value->permission->name == $request->get('permission')) {
                return response()->json(true);
            }
        }
        
        return response()->json(false);
    }

    public function changePassword(Request $request) {
        $userData = $request->get('userData');
        
        $user = User::find(Auth::user()->id);

        $user->password = Hash::make($userData['password']);
        $user->save();

        $returnData = Auth::user();

        return response()->json($returnData);
    }

    public function makeSchedule(Request $request) {
        $requestData = $request->get('requestData');

        $carbon = new Carbon($requestData['schedule']);
        $carbon->format('Y-m-d H:i:s');

        $complaint = Complaint::whereDate('schedule', $carbon)->get();

        if(count($complaint) < 3) {
            $complaintSched = Complaint::find($requestData['id']);
            $complaintSched->schedule = $carbon;
            $complaintSched->save();

            $returnData = Complaint::select('complaint AS title', 'schedule AS start')->get();

            return response()->json($returnData);
        }else {
            return response()->json(array('result' => "You day you want to schedule is already full"));
        }
    }

    public function insertAnnouncement(Request $request) {

        $announcementData = $request->get('announcementData');
        
        $announcement = new Announcement();

        $announcement->user_id = Auth::user()->id;
        $announcement->content = $announcementData['content'];
        $announcement->save();

        $returnData = Announcement::with('user.userPermissions.permission')
                    ->with('homeCarousells')
                    ->get();

        return response()->json($returnData);
    }

    public function displayAnnouncement(Request $request) {

        $announcementData = $request->get('announcementData');

        $homeCarousell = HomeCarousell::find(1);

        $homeCarousell->announcement_id = $announcementData['id'];
        $homeCarousell->save();

        $returnData = Announcement::with('user.userPermissions.permission')
                    ->with('homeCarousells')
                    ->get();

        return response()->json($returnData);
    }

    public function deleteAnnouncement(Request $request) {

        Announcement::destroy($request->get('announcementID'));

        $returnData = Announcement::with('user.userPermissions.permission')
                    ->with('homeCarousells')
                    ->get();

        return response()->json($returnData);
    }

    public function updateAnnouncement(Request $request) {

        $announcementData = $request->get('announcementData');
        
        $announcement = Announcement::find($announcementData['id']);

        $announcement->content = $announcementData['content'];
        $announcement->save();

        $returnData = Announcement::with('user.userPermissions.permission')
                    ->with('homeCarousells')
                    ->get();

        return response()->json($returnData);
    }

    public function addBudget(Request $request) {

        $formData = $request->get('formData');
        
        $budget = new DashboardBudget;

        $budget->user_id = Auth::user()->id;
        $budget->label = $formData['label'];
        $budget->value = $formData['value'];
        $budget->color = $formData['color'];
        $budget->save();

        $returnData = DashboardBudget::with('user.userPermissions.permission')
                    ->get();

        return response()->json($returnData);
    }

    public function updateBudget(Request $request) {

        $formData = $request->get('formData');
        
        $budget = DashboardBudget::find($formData['id']);

        $budget->label = $formData['label'];
        $budget->value = $formData['value'];
        $budget->color = $formData['color'];
        $budget->save();

        $returnData = DashboardBudget::with('user.userPermissions.permission')
                    ->get();

        return response()->json($returnData);
    }

    public function deleteBudget(Request $request) {

        DashboardBudget::destroy($request->get('budgetID'));

        $returnData = DashboardBudget::with('user.userPermissions.permission')
                    ->get();

        return response()->json($returnData);
    }

    public function addCrime(Request $request) {

        $formData = $request->get('formData');
        
        $crime = new DashboardCrime;

        $crime->user_id = Auth::user()->id;
        $crime->label = $formData['label'];
        $crime->value = $formData['value'];
        $crime->color = $formData['color'];
        $crime->save();

        $returnData = DashboardCrime::with('user.userPermissions.permission')
                    ->get();

        return response()->json($returnData);
    }

    public function updateCrime(Request $request) {

        $formData = $request->get('formData');
        
        $crime = DashboardCrime::find($formData['id']);

        $crime->label = $formData['label'];
        $crime->value = $formData['value'];
        $crime->color = $formData['color'];
        $crime->save();

        $returnData = DashboardCrime::with('user.userPermissions.permission')
                    ->get();

        return response()->json($returnData);
    }

    public function deleteCrime(Request $request) {

        DashboardCrime::destroy($request->get('budgetID'));

        $returnData = DashboardCrime::with('user.userPermissions.permission')
                    ->get();

        return response()->json($returnData);
    }

    public function addPopulation(Request $request) {

        $formData = $request->get('formData');
        
        $population = new DashboardPopulation();

        $population->user_id = Auth::user()->id;
        $population->label = $formData['label'];
        $population->value = $formData['value'];
        $population->color = $formData['color'];
        $population->save();

        $returnData = DashboardPopulation::with('user.userPermissions.permission')
                    ->get();

        return response()->json($returnData);
    }

    public function updatePopulation(Request $request) {

        $formData = $request->get('formData');
        
        $population = DashboardPopulation::find($formData['id']);

        $population->label = $formData['label'];
        $population->value = $formData['value'];
        $population->color = $formData['color'];
        $population->save();

        $returnData = DashboardPopulation::with('user.userPermissions.permission')
                    ->get();

        return response()->json($returnData);
    }

    public function deletePopulation(Request $request) {

        DashboardPopulation::destroy($request->get('budgetID'));

        $returnData = DashboardPopulation::with('user.userPermissions.permission')
                    ->get();

        return response()->json($returnData);
    }

    public function uploadImages(Request $request) {
        if($request->get('image1') && $request->get('image1') != '') {
            $image = $request->get('image1');
            $ext = explode('/', explode(':', substr($image, 0, strpos($image, ';')))[1])[1];
            // $name = time().'.' . explode('/', explode(':', substr($image, 0, strpos($image, ';')))[1])[1];
            \Image::make($request->get('image1'))->save(public_path('images/img1.').$ext);

            $caro = HomeCarousell::find(1);

            $caro->image1 = 'images/img1.' . $ext;
            $caro->save();
        }

        if($request->get('image2') && $request->get('image2') != '') {
            $image = $request->get('image2');
            $ext = explode('/', explode(':', substr($image, 0, strpos($image, ';')))[1])[1];
            // $name = time().'.' . explode('/', explode(':', substr($image, 0, strpos($image, ';')))[1])[1];
            \Image::make($request->get('image2'))->save(public_path('images/img2.').$ext);

            $caro = HomeCarousell::find(1);

            $caro->image2 = 'images/img2.' . $ext;
            $caro->save();
        }

        if($request->get('image3') && $request->get('image3') != '') {
            $image = $request->get('image3');
            $ext = explode('/', explode(':', substr($image, 0, strpos($image, ';')))[1])[1];
            // $name = time().'.' . explode('/', explode(':', substr($image, 0, strpos($image, ';')))[1])[1];
            \Image::make($request->get('image3'))->save(public_path('images/img3.').$ext);

            $caro = HomeCarousell::find(1);

            $caro->image3 = 'images/img3.' . $ext;
            $caro->save();
        }

        return response()->json(['success' => 'You have successfully uploaded an image'], 200);
    }

    public function getNotification(Request $request) {
        $notification = Notification::where('active', '=', 1)
                    ->with('user.userPermissions.permission')
                    ->get();

        return response()->json($notification);
    }

    public function getReportByDays(Request $request) {
        $reports = Report::whereDate('created_at', '>', Carbon::now()->subDays((int)$request->get('days')))
                ->get()
                ->groupBy('report_type');

        return response()->json($reports);
    }
}
