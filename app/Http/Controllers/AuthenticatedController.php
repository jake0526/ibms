<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use App\Permission;
use App\UserPermission;
use App\Report;
use App\Complaint;
use App\Announcement;
use App\DashboardBudget;
use App\DashboardCrime;
use App\DashboardPopulation;
use Auth;
use SebastianBergmann\CodeCoverage\Report\Html\Dashboard;
use App\Notification;

use Illuminate\Support\Carbon;

class AuthenticatedController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */

    public function userManagement() {
        $title = 'User Management';
        $page = 'user-manager';

        $isAdmin = false;

        $budget = DashboardBudget::with('user.userPermissions.permission')
                    ->get();
        $crime = DashboardCrime::with('user.userPermissions.permission')
                    ->get();
        $population = DashboardPopulation::with('user.userPermissions.permission')
                    ->get();
        $notification = Notification::where('active', '=', 1)
                    ->with('user.userPermissions.permission')
                    ->get();

        if (Auth::user()) {
            $userPermission = User::where('id', Auth::user()->id)->with('userPermissions.permission')->first();

            foreach ($userPermission->userPermissions as $value) {
                if ($value->permission->name == 'Admin') {
                    $isAdmin = true;
                }
            }
        }

        $reports = Report::whereDate('created_at', '>', Carbon::now()->subDays(7))
                ->get()
                ->groupBy('report_type');

        $userData = User::All();
        $permissions = Permission::All();

        return view('pages.manager.user', compact('title', 'page', 'userData', 'permissions', 'isAdmin', 'budget', 'crime', 'population', 'notification', 'reports'));
    }

    public function complaints(Request $request) {
        $title = 'Complaints';
        $page = 'complaints';

        if($request->exists('n')) {
            $notificationId = $request->input('n');

            $notification = Notification::find($notificationId);
            $notification->active = 0;
            $notification->save();
        }        

        $budget = DashboardBudget::with('user.userPermissions.permission')
                    ->get();
        $crime = DashboardCrime::with('user.userPermissions.permission')
                    ->get();
        $population = DashboardPopulation::with('user.userPermissions.permission')
                    ->get();
        $notification = Notification::where('active', '=', 1)
                    ->with('user.userPermissions.permission')
                    ->get();


        $isAdmin = false;

        if (Auth::user()) {
            $userPermission = User::where('id', Auth::user()->id)->with('userPermissions.permission')->first();

            foreach ($userPermission->userPermissions as $value) {
                if ($value->permission->name == 'Admin') {
                    $isAdmin = true;
                }
            }
        }

        $complaintData = Complaint::with('user')->get();
        $complaintSchedule = Complaint::select('respondent AS title', 'schedule AS start')->get();

        $reports = Report::whereDate('created_at', '>', Carbon::now()->subDays(7))
                ->get()
                ->groupBy('report_type');

        return view('pages.manager.complaints', compact('title', 'page', 'complaintData', 'complaintSchedule', 'isAdmin', 'budget', 'crime', 'population', 'notification', 'reports'));
    }

    public function reports(Request $request) {
        $title = 'Reports';
        $page = 'reports';

        if($request->exists('n')) {
            $notificationId = $request->input('n');

            $notification = Notification::find($notificationId);
            $notification->active = 0;
            $notification->save();
        }

        $isAdmin = false;

        $budget = DashboardBudget::with('user.userPermissions.permission')
                    ->get();
        $crime = DashboardCrime::with('user.userPermissions.permission')
                    ->get();
        $population = DashboardPopulation::with('user.userPermissions.permission')
                    ->get();
        $notification = Notification::where('active', '=', 1)
                    ->with('user.userPermissions.permission')
                    ->get();

        if (Auth::user()) {
            $userPermission = User::where('id', Auth::user()->id)->with('userPermissions.permission')->first();

            foreach ($userPermission->userPermissions as $value) {
                if ($value->permission->name == 'Admin') {
                    $isAdmin = true;
                }
            }
        }

        $reports = Report::whereDate('created_at', '>', Carbon::now()->subDays(7))
                ->get()
                ->groupBy('report_type');

        // if($request->exists('d')) {
        //     $reportDays = $request->input('d');
            
        //     $reportData = Report::with('user')
        //         ->whereDate('created_at', '>', Carbon::now()->subDays($reportDays))
        //         ->get();
        // }else {
        //     $reportDays = 7;
        //     $reportData = Report::with('user')
        //         ->whereDate('created_at', '>', Carbon::now()->subDays(7))
        //         ->get();
        // }

        if($request->exists('m') && $request->exists('y')) {
            $reportMonth = $request->input('m');
            $reportYear = $request->input('y');
            
            $reportData = Report::with('user')
                ->whereMonth('created_at', $reportMonth)
                ->whereYear('created_at', $reportYear)
                ->orderBy('created_at', 'DESC')
                ->get();
        }else {
            $now = Carbon::now();
            
            $reportMonth = $now->format('m');
            $reportYear = $now->format('Y');

            $reportData = Report::with('user')
                ->whereMonth('created_at', $reportMonth)
                ->whereYear('created_at', $reportYear)
                ->orderBy('created_at', 'DESC')
                ->get();
        }

        return view('pages.manager.reports', compact('title', 'page', 'reports', 'isAdmin', 'budget', 'crime', 'population', 'notification', 'reportData', 'reportMonth', 'reportYear'));
    }

    public function adminDashboard(Request $request) {
        $title = 'Admin Dashboard';
        $page = 'admin-dashboard';

        $budget = DashboardBudget::with('user.userPermissions.permission')
                    ->get();
        $crime = DashboardCrime::with('user.userPermissions.permission')
                    ->get();
        $population = DashboardPopulation::with('user.userPermissions.permission')
                    ->get();
        $notification = Notification::where('active', '=', 1)
                    ->with('user.userPermissions.permission')
                    ->get();

        if($request->exists('d')) {
            $reportDays = $request->input('d');

            $reports = Report::whereDate('created_at', '>', Carbon::now()->subDays($reportDays))
                ->get()
                ->groupBy('report_type');  
        }else {
            $reportDays = 7;
            $reports = Report::whereDate('created_at', '>', Carbon::now()->subDays(7))
                ->get()
                ->groupBy('report_type');
        }

        return view('pages.administrator.dashboard', compact('title', 'page', 'budget', 'crime', 'population', 'notification', 'reports', 'reportDays'));
    }

    public function adminDashboardReports() {
        $title = 'Reports Dashboard';
        $page = 'admin-dashboard';

        $budget = DashboardBudget::with('user.userPermissions.permission')
                    ->get();
        $crime = DashboardCrime::with('user.userPermissions.permission')
                    ->get();
        $population = DashboardPopulation::with('user.userPermissions.permission')
                    ->get();
        $notification = Notification::where('active', '=', 1)
                    ->with('user.userPermissions.permission')
                    ->get();
        
        $reports = Report::whereDate('created_at', '>', Carbon::now()->subDays(7))
                    ->get()
                    ->groupBy('report_type');

        return view('pages.administrator.report_chart', compact('title', 'page', 'budget', 'crime', 'population', 'notification', 'reports'));
    }

    public function adminDashboardReportsCrime() {
        $title = 'Report Crimes Dashboard';
        $page = 'admin-dashboard';

        $budget = DashboardBudget::with('user.userPermissions.permission')
                    ->get();
        $crime = DashboardCrime::with('user.userPermissions.permission')
                    ->get();
        $population = DashboardPopulation::with('user.userPermissions.permission')
                    ->get();
        $notification = Notification::where('active', '=', 1)
                    ->with('user.userPermissions.permission')
                    ->get();
        
        $reports = Report::whereDate('created_at', '>', Carbon::now()->subDays(7))
                    ->get()
                    ->groupBy('report_type');

        return view('pages.administrator.report_crime_chart', compact('title', 'page', 'budget', 'crime', 'population', 'notification', 'reports'));
    }

    public function adminDashboardBudget() {
        $title = 'Budget Dashboard';
        $page = 'admin-dashboard';

        $budget = DashboardBudget::with('user.userPermissions.permission')
                    ->get();
        $crime = DashboardCrime::with('user.userPermissions.permission')
                    ->get();
        $population = DashboardPopulation::with('user.userPermissions.permission')
                    ->get();
        $notification = Notification::where('active', '=', 1)
                    ->with('user.userPermissions.permission')
                    ->get();
        
        $reports = Report::whereDate('created_at', '>', Carbon::now()->subDays(7))
                    ->get()
                    ->groupBy('report_type');

        return view('pages.administrator.budget_chart', compact('title', 'page', 'budget', 'crime', 'population', 'notification', 'reports'));
    }

    public function adminDashboardCrime() {
        $title = 'Crime Dashboard';
        $page = 'admin-dashboard';

        $budget = DashboardBudget::with('user.userPermissions.permission')
                    ->get();
        $crime = DashboardCrime::with('user.userPermissions.permission')
                    ->get();
        $population = DashboardPopulation::with('user.userPermissions.permission')
                    ->get();
        $notification = Notification::where('active', '=', 1)
                    ->with('user.userPermissions.permission')
                    ->get();


        $reports = Report::whereDate('created_at', '>', Carbon::now()->subDays(7))
                    ->get()
                    ->groupBy('report_type');

        return view('pages.administrator.crime_chart', compact('title', 'page', 'budget', 'crime', 'population', 'notification', 'reports'));
    }

    public function adminDashboardPopulation() {
        $title = 'Population Dashboard';
        $page = 'admin-dashboard';

        $budget = DashboardBudget::with('user.userPermissions.permission')
                    ->get();
        $crime = DashboardCrime::with('user.userPermissions.permission')
                    ->get();
        $population = DashboardPopulation::with('user.userPermissions.permission')
                    ->get();
        $notification = Notification::where('active', '=', 1)
                    ->with('user.userPermissions.permission')
                    ->get();

        $reports = Report::whereDate('created_at', '>', Carbon::now()->subDays(7))
                    ->get()
                    ->groupBy('report_type');

        return view('pages.administrator.population_chart', compact('title', 'page', 'budget', 'crime', 'population', 'notification', 'reports'));
    }

    public function adminAnnouncement() {
        $title = 'Admin Announcement';
        $page = 'admin-announcement';

        $isAdmin = false;

        $budget = DashboardBudget::with('user.userPermissions.permission')
                    ->get();
        $crime = DashboardCrime::with('user.userPermissions.permission')
                    ->get();
        $population = DashboardPopulation::with('user.userPermissions.permission')
                    ->get();
        $notification = Notification::where('active', '=', 1)
                    ->with('user.userPermissions.permission')
                    ->get();


        if (Auth::user()) {
            $userPermission = User::where('id', Auth::user()->id)->with('userPermissions.permission')->first();

            foreach ($userPermission->userPermissions as $value) {
                if ($value->permission->name == 'Admin') {
                    $isAdmin = true;
                }
            }
        }

        $reports = Report::whereDate('created_at', '>', Carbon::now()->subDays(7))
                ->get()
                ->groupBy('report_type');

        $announcementData = Announcement::with('user.userPermissions.permission')
                            ->with('homeCarousells')
                            ->get();

        return view('pages.administrator.announcement', compact('title', 'page', 'announcementData', 'budget', 'crime', 'population', 'notification', 'reports'));
    }

    public function adminUserAndPermission() {
        $title = 'Admin User and Permission';
        $page = 'admin-user-permission';
        $userData = User::with('userPermissions.permission')->get();
        $permissions = Permission::All();

        $budget = DashboardBudget::with('user.userPermissions.permission')
                    ->get();
        $crime = DashboardCrime::with('user.userPermissions.permission')
                    ->get();
        $population = DashboardPopulation::with('user.userPermissions.permission')
                    ->get();
        $notification = Notification::where('active', '=', 1)
                    ->with('user.userPermissions.permission')
                    ->get();

        $reports = Report::whereDate('created_at', '>', Carbon::now()->subDays(7))
                    ->get()
                    ->groupBy('report_type');

        return view('pages.administrator.user_and_permission', compact('title', 'page', 'userData', 'permissions', 'budget', 'crime', 'population', 'notification', 'reports'));
    }

    public function userProfile() {
        $title = 'User Profile';
        $page = 'user-profile';
        $userData = User::with('userPermissions.permission')->get();
        $permissions = Permission::All();
        $userLogin = Auth::user();

        $isAdmin = false;

        $budget = DashboardBudget::with('user.userPermissions.permission')
                    ->get();
        $crime = DashboardCrime::with('user.userPermissions.permission')
                    ->get();
        $population = DashboardPopulation::with('user.userPermissions.permission')
                    ->get();
        $notification = Notification::where('active', '=', 1)
                    ->with('user.userPermissions.permission')
                    ->get();

        $reports = Report::whereDate('created_at', '>', Carbon::now()->subDays(7))
                    ->get()
                    ->groupBy('report_type');

        if (Auth::user()) {
            $userPermission = User::where('id', Auth::user()->id)->with('userPermissions.permission')->first();

            foreach ($userPermission->userPermissions as $value) {
                if ($value->permission->name == 'Admin') {
                    $isAdmin = true;
                }
            }
        }

        return view('pages.profile.index', compact('title', 'page', 'userData', 'permissions', 'isAdmin', 'userLogin', 'budget', 'crime', 'population', 'notification', 'reports'));
    }
}
