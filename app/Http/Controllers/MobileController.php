<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

use App\User;
use App\Permission;
use App\Report;
use App\Complaint;
use App\Notification;
use App\HomeCarousell;
use App\Announcement;
use App\DashboardBudget;
use App\DashboardCrime;
use App\DashboardPopulation;
use Hash;

use Illuminate\Support\Carbon;

class MobileController extends Controller
{
    public function authLogin(Request $request) {
        $username = $request->get('username');
        $password = $request->get('password');

        $userData = User::where('username', $username)->first();

        $checkHash = Hash::check($password, $userData['password']);
        if ($checkHash) {
            return response()->json($userData);
        }
        return response()->json(false);
    }

    public function permissions(Request $request) {
        $permissionList = Permission::all();

        return response()->json($permissionList);
    }

    public function submitReport(Request $request) {

        // Reports Data

        $userId = $request->get('userId');
        $locationLat = $request->get('locationLat');
        $locationLng = $request->get('locationLng');
        $description = $request->get('description');
        $type = $request->get('type');
        $imageCount = $request->get('imageCount');

        // Push Notification Data

        $uploadedFile1 = $request->file('file0');
        
        $filename1 = time().$uploadedFile1->getClientOriginalName();

        Storage::disk('public')->putFileAs(
            'files/reports/',
            $uploadedFile1,
            $filename1
        );

        $filename = 'files/reports/' . $filename1 . '|';

        if($imageCount == 2) {
            $uploadedFile2 = $request->file('file1');

            $filename2 = time().$uploadedFile2->getClientOriginalName();
    
            Storage::disk('public')->putFileAs(
                'files/reports/',
                $uploadedFile2,
                $filename2
            );

            $filename .= 'files/reports/' . $filename2;
        }
        
        $report = new Report;
        
        $report->user_id = $userId;
        $report->description = $description;
        $report->report_type = $type;
        $report->image_path = $filename;
        $report->lat = $locationLat;
        $report->lng = $locationLng;
        $report->save();

        $userData = User::where('id', $userId)->first();

        $notification = new Notification;

        $notification->user_id = $userId;
        $notification->details = "New Reports from " . $userData['firstname'] . " " . $userData['lastname'];
        $notification->link = "/reports";
        $notification->active = 1;
        $notification->save();

        $data = ['success' => true];

        return response()->json($data);
    }

    public function submitComplaint(Request $request) {
        $userId = $request->get('userId');
        $complaintData = $request->get('complaint');
        $respondent = $request->get('respondent');
        $address = $request->get('address');

        $complaint = new Complaint;

        $complaint->user_id = $userId;
        $complaint->complaint = $complaintData;
        $complaint->respondent = $respondent;
        $complaint->address = $address;
        $complaint->save();

        $userData = User::where('id', $userId)->first();

        $notification = new Notification;

        $notification->user_id = $userId;
        $notification->details = "New Complaint from " . $userData['firstname'] . " " . $userData['lastname'];
        $notification->link = "/complaints";
        $notification->active = 1;
        $notification->save();

        $data = ['success' => true];

        return response()->json($data);
    }

    public function pushNotification(Request $request) {
        $userId = $request->get('userId');
        $details = $request->get('details');
        $link = $request->get('link');
        $active = $request->get('active');

        $notification = new Notification;

        $notification->user_id = $userId;
        $notification->details = $details;
        $notification->link = $link;
        $notification->active = $active;
        $notification->save();

        $data = ['success' => true];
        
        return response()->json($data);
    }

    public function homeCarousell(Request $request) {
        $homeController = HomeCarousell::with('announcement')->first();

        return response()->json($homeController);
    }

    public function dashboardCharts(Request $request) {
        $reportDays = $request->get('days');

        $report = Report::whereDate('created_at', '>', Carbon::now()->subDays((int)$reportDays))
                ->get()
                ->groupBy('report_type');
        $budget = DashboardBudget::with('user.userPermissions.permission')
                    ->get();
        $crime = DashboardCrime::with('user.userPermissions.permission')
                    ->get();
        $population = DashboardPopulation::with('user.userPermissions.permission')
                    ->get();

        $dataReport = array(
            'label' => "",
            'value' => "",
            'color' => ""
        );

        $reportComplaintArray = [];

        $colorDefaultList = array(  
            'Noise' => '#66a3ff',
            'Water Interruption' => '#d966ff',
            'Electrical Interruption' => '#c266ff',
            'Flood' => '#f442dc',
            'Others' => '#ff66a3'
        );

        foreach ($report as $key=>$item) {
            if($key == 'Noise' || $key == 'Water Interruption' || $key == 'Electrical Interruption' || $key == 'Flood' || $key == 'Others'){
                $dataReport['label'] = $key;
                $dataReport['value'] = count($item);
                $dataReport['color'] = $colorDefaultList[$key];

                array_push($reportComplaintArray, $dataReport);
            }
        }

        $dataReport = array(
            'label' => "",
            'value' => "",
            'color' => ""
        );

        $reportCrimeArray = [];

        $colorDefaultList = array(  
            'Violence' => '#8cd98c',
            'Rape' => '#258c10',
            'Robbery' => '#737c71',
            'Drugs' => '#078269',
            'Murder' => '#071582'
        );

        foreach ($report as $key=>$item) {
            if($key == 'Violence' || $key == 'Rape' || $key == 'Robbery' || $key == 'Drugs' || $key == 'Murder'){
                $dataReport['label'] = $key;
                $dataReport['value'] = count($item);
                $dataReport['color'] = $colorDefaultList[$key];

                array_push($reportCrimeArray, $dataReport);
            }
        }

        $data = ['budget' => $budget, 'crime' => $crime, 'population' => $population, 'reportComplaint' => $reportComplaintArray, 'reportCrime' => $reportCrimeArray, 'reportDays' => $reportDays];

        return response()->json($data);
    }
}
