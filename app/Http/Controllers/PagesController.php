<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\User;
use App\HomeCarousell;
use App\DashboardBudget;
use Auth;
use App\DashboardCrime;
use App\DashboardPopulation;
use App\Notification;
use App\UserPermission;
use App\Report;

use Illuminate\Support\Carbon;

class PagesController extends Controller
{
    public function index() {
        $title = 'Home';
        $page = 'home';
        $isAdmin = false;
        $hasReport = false;
        $hasComplaint = false;

        $budget = DashboardBudget::with('user.userPermissions.permission')
                    ->get();
        $crime = DashboardCrime::with('user.userPermissions.permission')
        ->get();
        $population = DashboardPopulation::with('user.userPermissions.permission')
        ->get();
        $notification = Notification::where('active', '=', 1)
        ->with('user.userPermissions.permission')
        ->get();

        if (Auth::user()) {
            $userPermission = User::where('id', Auth::user()->id)->with('userPermissions.permission')->first();

            foreach ($userPermission->userPermissions as $value) {
                if ($value->permission->name == 'Admin') {
                    $isAdmin = true;
                }else if ($value->permission->name == 'Incident Report') {
                    $hasReport = true;
                }else if ($value->permission->name == 'Complaints') {
                    $hasComplaint = true;
                }
            }
        }

        $reports = Report::whereDate('created_at', '>', Carbon::now()->subDays(7))
                ->get()
                ->groupBy('report_type');

        $homeController = HomeCarousell::with('announcement')->first();

        return view('pages.index', compact('title', 'page', 'isAdmin', 'hasReport', 'hasComplaint', 'homeController', 'budget', 'crime', 'population', 'notification', 'reports'));
    }
}
